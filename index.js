const express = require('express');
const Provider = require('oidc-provider');
const { ApolloServer, gql } = require('apollo-server-express');

const app = express();


const clients = [{
    client_id: 'DBDApp',
    grant_types: ['implicit'],
    response_types: ['id_token'],
    redirect_uris: ['https://192.168.1.36:3000'],
    token_endpoint_auth_method: 'none'
}];
 

const oidc = new Provider('http://localhost:4000', {
    claims: {
        email: ['email', 'email_verified'],
        phone: ['phone_number', 'phone_number_verified'],
        profile: ['birthdate', 'family_name', 'gender', 'given_name', 'locale', 'middle_name', 'name', 'nickname', 'picture', 'preferred_username', 'profile', 'updated_at', 'website', 'zoneinfo']
    },
    features: {
        sessionManagement: true
    }
});

oidc.initialize({clients}).then(function () {
    app.use('/', oidc.callback);
    app.listen(4000);
});